
public class ArrayDeletionsLab2_1 {
    public static void main(String[] args) throws Exception {
        int[] someArray = { 1, 4, 5, 2, 1 };
        someArray = deleteElementByIndex(someArray, 1);
        printArrayInt(someArray);
        someArray = deleteElementByValue(someArray, 1);
        System.out.println();
        printArrayInt(someArray);

    }

    private static int[] deleteElementByValue(int[] arr, int value) {
        int[] newArr = new int[arr.length - 1];
        int index = 0;
        boolean valueFound = false;
        for (int e : arr) {
            if (e == value && !valueFound) {
                valueFound = true;
            } else {
                newArr[index] = e;
                index++;
            }
        }
        if (!valueFound) {
            return arr;
        }
        return newArr;
    }

    private static void printArrayInt(int[] arr) {
        for (int e : arr) {
            System.out.print(" " + e);
        }
    }

    private static int[] deleteElementByIndex(int[] arr, int index) {
        int[] newArr = new int[arr.length - 1];
        for (int i = 0; i < index; i++) {
            newArr[i] = arr[i];
        }
        for (int i = index + 1; i < arr.length; i++) {
            newArr[i - 1] = arr[i];
        }
        return newArr;
    }
}
