public class ArrayDeletionsLab2_2 {
    public static void main(String[] args) throws Exception {
        int[] nums = { 3, 2, 2, 3 };
        int k = removeElement(nums, 3);
        System.out.println("k = " + k);
    }

    private static int removeElement(int[] arr, int value) {
        int k = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != value) {
                arr[k] = arr[i];
                k++;
            } else {
                arr[i] = '_';
            }
        }
        return k;
    }

}
