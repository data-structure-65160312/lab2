public class ArrayDeletionsLab2_3 {
    public static void main(String[] args) throws Exception {
        int[] nums = {1,1,2};
        int k = removeDuplicates(nums);
        System.out.println("k = " + k);
    }
    private static int removeDuplicates(int[] arr) {
        int k = 1;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != arr[i-1]) {
                arr[k] = arr[i];
                k++;
            } 
        }
        return k;
    }
}
